# Yurt

This application is meant as an experiment for a certain architecture I want to try out, and, more generally, to understand and figure out good practices more.

A talk which gave me the idea for this architecture is [The Principles of Clean Architecture by Uncle Bob Martin](https://invidious.fdn.fr/watch?v=o_TH-Y78tt4). José Valim, the person who created Elixir, also wrote a good [article on testing and mocks](https://dashbit.co/blog/mocks-and-explicit-contracts), whose good practices I try to follow.

## Architecture

Instead of starting with a web-and-database application as a framework where we build our own application in, the goal is to write our application and have input and storage *behaviour* explicitly defined in *interfaces*. These interfaces are the *boundaries* of our *application*. Other boundaries can also be defined, most notably every in- and output, or more generally, side effects, should be clearly separated from the application by it's own boundary. Modules can then be written by implementing the behaviour of the interfaces who mark the boundaries, and these modules can be enabled in settings.

![Visual representation of such architecture](README/architecture.png "Architecture example of a fediverse enabled social network platform")

Development happens by incrementally adding *features*. These features can be described in a *use-case*. A use-case in this context, is a high level description of the *data* that's involved, the *happy-flow*, and the *exception-flow*. A use case can also grow or change over time. Development happens *TTD*.

Note that not everything is a use-case. Maybe you implemented a feature, but some business rules change, like passwords now need to be longer. Or maybe a feature needs to be expanded, buut it's not really a new thing. Or maybe there's a bug that needs fixing. Or you want to refactor some code. I'm unsure how to call those, but let's be consious about the fact that they are there as well.

## Tests
### Unit tests

*Unit tests* are only written against the boundaries of the application and test what they are supposed to test, and not more. That way, the boundaries are strongly defined, but everything in between can be refactored and restructured without ever needing to touch the tests. The tests are grouped per use-case. In this context, a unit is the code between the boundaries. For example, the core part is a unit, the storage part is another.

There may be exceptions where you want to test a very specific piece of complex functionality. This can be considered a *micro-unit test*. I guess this is to be avoided and should at least be clearly marked as such in comments.

### Integration tests

Even when unit tests work, we can't exactly be sure that everything works nicely together. For this we need some integration tests. I'm not really sure what good practices are in that regard, however, so we need to figure this out as well.

From what I currently find online, they should eventually test the whole application with everything coupled. But it's allowed to have this happening in steps where you first add only one module, and when that works, add another. You can go bottom-up, top-down, sandwich style, or just do everything together in one big bang.

These tests aren't meant to test functionality, but rather test the plumbing. So I guess we don't need to redo all functionality of the unit tests, but at least passing through all functions, and trying different type of responses, is a must. For smaller applications the *big bang* approach seems the best one.

## Code comments

Code comments are also an important part of the code and something many people don't have good practices around. From my own experience, I feel that comments should be there when things are done in a surprising way. One day in the future someone may stumble on it and it may not be immediately apparent why something is done in this weird way, and they will have a lot of work trying to figure it out. Or maybe someone won't realise that something was done in a way that's not conventional, and bad practices will grow. In those cases, make sure it's clear from a comment.

Some examples where code comments are a must:

* Good practices aren't followed. Make sure it's clear why and maybe comment on how it could be done better in the future.
* You had to work around an upstream issue or bug. Make this clear, make it clear how you work around it and how it can be done if the problem wasn't there, and provide a link to the issue report. Make sure there's at least enough context so that someone in the future can easily figure out if upstream has fixed it and what they need to do to clean up the code.
* Some code was added for backwards compatibility. Make sure to make it clear what it was for with enough information so it can later be decided if it can be removed or not. Also make sure it's clear what needs to be done to remove it.
* A piece of code is quite complex, so some explanation can help understand it better.
* You do something in a certain way, but the reasoning may not be immediately clear why it's done this way.

## Logging

Good logging is essential in case of bugs or problems. I'm unsure what good practices are in that regard, so for now, I'm not thinking too much about it. At the very least, logging should not be part of the core application, is therefor a boundary, and therefor requires an interface. Do not call a logging library directly from your application.

## Example

Here I go over the process I did to get a first grasp. Some things I go over quickly. Simply following this wont give you an actual working application, but it should be enough to understand *why* things are happening in a certain way. To have a working example, check the code.

### Use-cases

To give an example of how we want to work with use-cases, let's take the use-case for a registration flow:

* Data:
  * email
  * username
  * password
* Happy flow:
  1. User provides data
  1. Application validates data
  1. The new account is stored
  1. A success response is given
* Exception flow:
  1. When validation fails, a response is provided explaining what went wrong
  1. When something else went wrong, a response is provided explaining what went wrong

We have two boundaries, the input API and the storage. Let's start by setting up a structure for this. I'm not sure about good practices, so let's just do something, and go from there.

```
lib/yurt/api.ex
lib/yurt/storage.ex
```

Alright, now let's write the test for the happy flow!

test/api_test.exs
```elixir
defmodule Yurt.APITest do
  use ExUnit.Case

  describe "Registering an account" do
    # Happy flow
    test "stores a new account and provides an :ok response" do
      assert Yurt.API.register_account("UwU_01", "supertrooper", "my@me") == :ok
      assert_received {:store_new_account, "UwU_01", "supertrooper", "my@me"}
    end
  end
end
```

As you can see, we expect a function `Yurt.API.register_account/3` where we provide a username, password and mail address, and we expect it to return `:ok`. Note that for this example we stored the password, in practice you will obviously use a proper hash function and store the result of that, but we can add this later. Besides just returning an `:ok` status, we also want to be sure that the storage module was called. For that we use a neat Elixir trick. Elixir can send messages to processes, and since everything runs in a process, we can send a message to the process our test is running in. Here we check if we received a message after calling that function. Sending that message is not a part of the application, but we can add in to storage module that we use for testing. You can run this test with `elixir test/api_test.exs` or `mix test`, and it will fail. So let's get it to pass!

We need some configuration to say what modules to use. Elixir provides this for us, so we'll use that. Then we need to define our storage behaviour and write a mock implementation to use during the tests. Then we can define the API behaviour and implement our `Yurt.API.register_account/3` function.

config/config.exs
```elixir
import Config

config :yurt,
  storage_module: Yurt.Storage.StorageMock
```

lib/yurt/storage.ex
```elixir
defmodule Yurt.Storage do
  @callback store_new_account(String.t(), String.t(), String.t()) :: :ok
end
```

test/mocks/storage/storage_mock.ex
```elixir
defmodule Yurt.Storage.StorageMock do
  @behaviour Yurt.StorageBehaviour

  @impl Yurt.StorageBehaviour
  def store_new_account(username, password, mail_address) do
    send(self(), {:store_new_account, username, password, mail_address})
    :ok
  end
end
```

lib/yurt/api.ex
```elixir
defmodule Yurt.APIBehaviour do
  @callback register_account(String.t(), String.t(), String.t()) :: :ok
end

defmodule Yurt.API do
  @behaviour Yurt.APIBehaviour

  @storage_module Application.compile_env!(:yurt, :storage_module)

  @impl Yurt.APIBehaviour
  def register_account(username, password, email) do
    @storage_module.store_new_account(username, password, email)
  end
end
```

To actually get it working, we also need to start ExUnit, and to get it working with `mix test`, we also need to define the helper file. This helper file is typically used to start ExUnit, but can be used for other things as well. Then we also have to tell it to load the files, not just in lib, but also in test/mocks.

test/test_helper.exs
```elixir
ExUnit.start()
```

mix.exs (here I only show the relevant parts, this file should be generated with `mix new project_name`)
```elixir
defmodule Yurt.MixProject do
  use Mix.Project

  def project do
    [
      ...
      elixirc_paths: elixirc_paths(Mix.env()),
      ...
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/mocks"]
  defp elixirc_paths(_), do: ["lib"]

  ...

end
```

Now it should pass with `mix test`. Or, if you want to see it a bit more manually, do `elixir test/test_helper.exs test/api_test.exs`. To try the application manually, you can run it with `iex -S mix` and do e.g. `Yurt.register("me@wow", "me_me","123")`.

Now the rest of the use-case can be implemented by first adding a test, then making it properly pass. New use-cases can be added in a similar way as we've done here. And, while I didn't add this here, you'll probably gonna want to write [proper documentation](https://hexdocs.pm/elixir/1.12/writing-documentation.html) as well.

### Modules
#### Storage module
We now have an input boundary, and a boundary for storage. In production, we want to get the storage working with a database. We need to set up a bunch of things for that. We need to pull in dependencies, we have configuration, our supervision tree needs to start things, and so on. I'm not sure about good practices. If we take the loose coupling to the extremes, all of that should somehow be defined from the storage module itself. For now, lets be less extreme. At the very least we want to keep a good folder structure, which is slightly different from what a typical Phoenix app would do. To get Ecto working, you can follow <http://learningelixir.joekain.com/setting-up-ecto-in-elixir/>. The biggest change is that we place repo.ex at *lib/storage/repo.ex*, and the schemas in *lib/storage/repo/*. To get the database users working, you can check [how it's done in Akkoma](https://docs.akkoma.dev/stable/development/setting_up_akkoma_dev/).

lib/storage/repo.ex
```elixir
defmodule Yurt.Storage.Repo do
  use Ecto.Repo,
    otp_app: :yurt,
    adapter: Ecto.Adapters.Postgres
end
```

lib/storage/repo/account.ex
```elixir
defmodule Yurt.Storage.Repo.Account do
  use Ecto.Schema

  schema "account" do
    field(:username, :string)
    field(:password_hash, :string)
    field(:mail_address, :string)

    create(unique_index(:accounts, [:username]))
    create(unique_index(:accounts, [:mail_address]))
  end
end
```

We also want different configuration for testing. We do that by overloading the default config with config based on the environment.

config/config.exs
```elixir
import Config

config :yurt,
  storage_module: Yurt.Storage.StorageMock

config :yurt, Yurt.Storage.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "yurt",
  username: "yurt",
  password: "boopadoop"

config :yurt, ecto_repos: [Yurt.Storage.Repo]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
```

config/test.exs
```elixir
import Config

config :yurt,
  storage_module: Yurt.Storage.StorageMock

config :yurt, Yurt.Storage.Repo,
  adapter: Ecto.Adapters.Postgres,
  database: "yurt_test",
  username: "yurt",
  password: "boopadoop"
```

config/dev.exs
```elixir
import Config
```

Once all of this has been set up, we can store something in the database directly using `iex -S mix`

```
Yurt.Storage.Repo.insert(%Yurt.Storage.Repo.Account{username: "tumtum"})
```

Of course, we don't want to store things into the database directly; All of this was just plumbing to get the database available to us. We haven't even added extra functionality yet, because otherwise we would've written some tests. What we want to do now, is implement the storage behaviour, so that the database can be used as part of our application, while remaining only loosely coupled. For that we start by writing tests on the boundary to the Repo module.

We can take the Repo module as the boundary. This means that we need to implement the storage behaviour there. We start with writing a test. First we make sure there's no data in the database, and we also make sure we clean up after ourselves. Then we do the actual test. We check that there's no account, then we add one, now we check that there's exactly one account with the values we provided.

yurt/test/storage/repo_test.exs
```elixir
defmodule Yurt.Storage.RepoTest do
  use ExUnit.Case

  setup do
    Yurt.Storage.Repo.delete_all(Yurt.Storage.Repo.Account)
    on_exit(fn -> Yurt.Storage.Repo.delete_all(Yurt.Storage.Repo.Account) end)
    :ok
  end

  describe "Storing an account" do

    # Happy flow
    test "stores a new account and provides an :ok response" do
      # WARNING! Strongly coupled test!
      # At the time of writing this test, we didn't have a way to check through the Repo behaviour if an account was stored.
      # For now we check in the database directly, but that means our test is strongly coupled to the implementation, and that's bad practice.
      # Other options may be:
      # * If one day we have a function to fetch an account, we can maybe use that.
      # * Or maybe we can try to insert the same account again and check that we're not getting an `:ok` any more?
      assert Yurt.Storage.Repo.all(Yurt.Storage.Repo.Account) == []

      assert Yurt.Storage.Repo.store_new_account(
               "UwU_01",
               "supertrooper",
               "my@me"
             ) == :ok

      assert match?(
               [
                 %Yurt.Storage.Repo.Account{
                   username: "UwU_01",
                   password_hash: "supertrooper",
                   mail_address: "my@me"
                 }
               ],
               Yurt.Storage.Repo.all(Yurt.Storage.Repo.Account)
             )
    end
  end
end
```

Note that we have now strongly coupled our test to the application. Instead of working on the boundaries, we check if an account is created by querying it directly from the database. I don't think this is good practice, and we should probably check this with a function from the Storage behaviour. For now let's leave it as is, but add a comment.

Now that we have a test, we can implement it. We already created the accounts table and even have a module for it. We just need to make sure we define the Repo module to be using the Storage behaviour and then implement the last needed bits.

yurt/lib/storage/repo.exs
```elixir
defmodule Yurt.Storage.Repo do
  use Ecto.Repo,
    otp_app: :yurt,
    adapter: Ecto.Adapters.Postgres

  @behaviour Yurt.Storage

  @impl true
  def store_new_account(username, password_hash, mail_address) do
    Yurt.Storage.Repo.insert(%Yurt.Storage.Repo.Account{
      username: username,
      password_hash: password_hash,
      mail_address: mail_address
    }

    :ok
  end
end
```

Just like before, we only have the happy flow now. The rest can be added by writing a test for it and then impelmenting it. Good luck!

Now we can try this in real life. First we need to make sure we use the correct module in a non-test environment by setting the correct module in the general config. Make sure you still keep the mock in the test config!

config/config.exs
```elixir

...
config :yurt,
  storage_module: Yurt.Storage.Repo
...

```

Then we can start with `iex -S mix` and test it out. After running the follwoing, you should find the record in the database, hooray!

```
Yurt.API.register_account("betty", "boop oop a doop", "dizzy@dishes")
```

It's important to note that, while we test the storage module, the rest of the tests keep using the module that's defined in settings. And that's the mock we created. By adding things to a database, we are changing global state. Tests from different modules can run concurrently, meaning that changes in global state can leak to other tests. This can cause *flaky tests*. Those are tests who randomly fail at times, but pass at other times. Since we keep the changing of state limited to the database, and since the rest of the application doesn't have any interaction with the database, we can be more certain that our tests will remain stable. And if something does go wrong, we have a much smaller set of places where collisions like this can happen.

#### Input module

The next step is adding the user interface. In this case we just want an API interface, no web pages. You can generate a new web project using Phoenix, but then we have the problem that we get a framework where we build our application "in", while we want our framework to hook onto our application. We can't completely get away from this, but we can at least put the modules under a correct namespace and folder structure. Phoenix allows us to specify a module name, so let's do that.

```
mix phx.new --no-tailwind --no-mailer --no-html --no-gettext --no-assets --no-dashboard --module 'Yurt.API.RESTAPI' yurt
```

This gives us a new application and the whole thing is under `Yurt.API.RESTAPI` and `Yurt.API.RESTAPIWeb`. I decide to move the core things out, and then put our modules in. Some things, like the config, we'll need to merge somehow. I start by moving the files in the new project to the places I want them to be. That way nothing should braek. Once I checked that this works, I change the module names and references to them. And, huh, it turns out that the things I wanted to keep as "input module" are actually the things under `Yurt.API.RESTAPIWeb`. Ok, then. I'll just keep that name. Now that this works, let's merge the applications. One thing I notice is that Phoenix loads "test/support" where I loaded "test/mocks". It makes sense to me to have one folder to load and put everything in that folder. So I'll place the mocks folder there. I also noticed that Phoenix has it's own "application.ex", while I used "yurt.ex" for that. This seems reasonable to me, so I'll keep that as well. The last thing is that Phoenix uses a sandbox for database queries. It means we don't have to delete everything during setup, because things will now run in transactions, but we need some other setup.

Our lib and test file structure now looks something like this

```
+lib
|  +yurt
|  |   +api
|  |   |   +...
|  |   +storage
|  |   |   +...
|  |   +api.ex
|  |   +application.ex
|  |   +storage.ex
|  +yurt.ex
+test
   +restapi_web
   |   +...
   +storage
   |   +repo_test.exs
   +support
   |   +mocks
   |   |   +...
   |   +data_case.ex
   |   +conn_case.ex
   +api_test.exs
   +test_helper.exs
```

Note that the test folder doesn't completely resemble the lib folder. It still does in a way, but not completely. I'm unsure what to do with this. I don't think it's a good idea to mirror it too much, because I fear it will cause non-boundary modules to be added and tested on.

The next step is to write a test and code for the input module in a similar way like we did for the storage module. But instead of calling our core module, we should write a mock module for it and use that. Once the test passes, we still need to check if the modules work nicely together. This can be done by adding an integration test. We don't need to test all functionality, just the plumbing. In this case I would write one test who creates an account, and then creates another one with the same values. That way all modules will have managed a succes response and an error response.

I won't do that now, and leave them for the reader ;)

## License

    An experiment in architecture
    Copyright (C) 2023  ilja@ilja.space

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
