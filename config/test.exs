import Config

config :yurt,
  storage_module: Yurt.Storage.StorageMock

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :yurt, Yurt.Storage.Repo,
  adapter: Ecto.Adapters.Postgres,
  username: "yurt",
  password: "boopadoop",
  hostname: "localhost",
  database: "yurt_test#{System.get_env("MIX_TEST_PARTITION")}",
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :yurt, Yurt.API.RESTAPIWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "YwY4Y7QXFqdk17EVJUG+O6r5l4A/Mi9yuYpDkbS+0Ij6CP1uZ6GBNw2HopUM0ToC",
  server: false

# Print only warnings and errors during test
config :logger, level: :warning

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
