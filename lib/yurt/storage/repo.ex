defmodule Yurt.Storage.Repo do
  @moduledoc """
  Documentation for `Yurt.Storage.Repo`. This module is responsible for storing data in a database. The behaviour is defidned in `Yurt.Storage`.
  """
  use Ecto.Repo,
    otp_app: :yurt,
    adapter: Ecto.Adapters.Postgres

  @behaviour Yurt.Storage

  @doc """
  Stores a new account in the database

  Returns :ok or {:error, error_code}

  ## Examples

      iex> Yurt.Storage.Repo.store_new_account("and_i", "me, myself, and I", "me@myself")
      :ok

  """
  @impl true
  def store_new_account(username, password_hash, mail_address) do
    changeset =
      %Yurt.Storage.Repo.Account{
        username: username,
        password_hash: password_hash,
        mail_address: mail_address
      }
      |> Ecto.Changeset.change()
      |> Ecto.Changeset.unique_constraint(:username)
      |> Ecto.Changeset.unique_constraint(:mail_address)

    with {:ok, _account} <-
           Yurt.Storage.Repo.insert(changeset) do
      :ok
    else
      {:error,
       %{
         errors: [
           username:
             {"has already been taken",
              [constraint: :unique, constraint_name: "accounts_username_index"]}
         ]
       }} ->
        {:error, :username_already_exists}

      {:error,
       %{
         errors: [
           mail_address:
             {"has already been taken",
              [constraint: :unique, constraint_name: "accounts_mail_address_index"]}
         ]
       }} ->
        {:error, :mail_address_already_exists}

      # Note that this isn't tested.
      # When an unexpected error happens, we want to know it. But I can't really test unexpected, now can I.
      error ->
        {:error, {:undefined_by_yurt, error}}
    end
  end
end
