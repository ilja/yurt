defmodule Yurt.Storage.Repo.Account do
  use Ecto.Schema

  schema "accounts" do
    field(:username, :string)
    field(:password_hash, :string)
    field(:mail_address, :string)
  end
end
