defmodule Yurt.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      Yurt.API.RESTAPIWeb.Telemetry,
      Yurt.Storage.Repo,
      {DNSCluster, query: Application.get_env(:yurt, :dns_cluster_query) || :ignore},
      {Phoenix.PubSub, name: Yurt.API.RESTAPI.PubSub},
      # Start a worker by calling: Yurt.API.RESTAPI.Worker.start_link(arg)
      # {Yurt.API.RESTAPI.Worker, arg},
      # Start to serve requests, typically the last entry
      Yurt.API.RESTAPIWeb.Endpoint
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Yurt.API.RESTAPI.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    Yurt.API.RESTAPIWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
