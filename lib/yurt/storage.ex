defmodule Yurt.Storage do
  @moduledoc """
  Here we define the behaviour of a storage module
  """

  @doc """
  Store a new account.

  Returns :ok when the account is succesfully stored. When an error happen, return {:error, error_code}.

  email and username must be unique. When they are asked to be stored, return {:error, :username_already_exists} and {:error, :mail_address_already_exists} respectively.

  Parameters:
    mail address,
    username,
    password_hash

  ## Examples

      iex> Yurt.API.register_account("and_i", password_hash, "me@myself")
      :ok

  """
  @callback store_new_account(String.t(), String.t(), String.t()) :: :ok | {:error, String.t()}
end
