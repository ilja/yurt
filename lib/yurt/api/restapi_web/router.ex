defmodule Yurt.API.RESTAPIWeb.Router do
  use Yurt.API.RESTAPIWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", Yurt.API.RESTAPIWeb do
    pipe_through :api
  end
end
