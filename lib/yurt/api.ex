defmodule Yurt.APIBehaviour do
  @moduledoc """
  This is the behaviour of the API. All incomming interaction should use this behaviour.
  """

  @doc """
  See `Yurt.API.register_account/3` for details.
  """
  @callback register_account(String.t(), String.t(), String.t()) :: :ok
end

defmodule Yurt.API do
  @moduledoc """
  Documentation for `Yurt.API`. All incomming interaction should pass this module. The behaviour is defidned in `Yurt.APIBehaviour`.
  """
  @behaviour Yurt.APIBehaviour

  defp storage_module() do
    Application.fetch_env!(:yurt, :storage_module)
  end

  @doc """
  Validates input and registers a new account.

  Returns :ok or {:error, error_code}

  ## Examples

      iex> Yurt.API.register_account("and_i", "me, myself, and I", "me@myself")
      :ok

  """
  @impl Yurt.APIBehaviour
  def register_account(username, password, email) do
    with :ok <- password_validation(password),
         :ok <- mail_address_validation(email),
         :ok <- username_validation(username) do
      storage_module().store_new_account(username, password, email)
    else
      {:error, errorcode} -> {:error, errorcode}
    end
  end

  defp password_validation(password) do
    with true <- String.length(password) >= 12 do
      :ok
    else
      _ -> {:error, :password_validation_failed}
    end
  end

  defp mail_address_validation(mail_address) do
    with true <- Regex.match?(~r/.+@.+/, mail_address) do
      :ok
    else
      _ -> {:error, :mail_address_validation_failed}
    end
  end

  defp username_validation(username) do
    with true <- Regex.match?(~r/^[a-z,A-Z,0-9,_]+$/, username) do
      :ok
    else
      _ -> {:error, :username_validation_failed}
    end
  end
end
