defmodule Yurt.Storage.Repo.Migrations.AddAccountSchema do
  use Ecto.Migration

  def change do
    create table(:accounts) do
      add(:username, :string)
      add(:password_hash, :string)
      add(:mail_address, :string)
    end

    create(unique_index(:accounts, [:username]))
    create(unique_index(:accounts, [:mail_address]))
  end
end
