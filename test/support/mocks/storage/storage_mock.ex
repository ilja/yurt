defmodule Yurt.Storage.StorageMock do
  @behaviour Yurt.Storage

  @impl true
  def store_new_account(username = "existing_user", password_hash, mail_address) do
    send(self(), {:store_new_account, username, password_hash, mail_address})
    {:error, :username_already_exists}
  end

  def store_new_account(username, password_hash, mail_address = "existing@example.org") do
    send(self(), {:store_new_account, username, password_hash, mail_address})
    {:error, :mail_address_already_exists}
  end

  def store_new_account(username, password_hash, mail_address) do
    send(self(), {:store_new_account, username, password_hash, mail_address})
    :ok
  end
end
