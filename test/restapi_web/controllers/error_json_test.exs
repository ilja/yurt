defmodule Yurt.API.RESTAPIWeb.ErrorJSONTest do
  use Yurt.API.RESTAPIWeb.ConnCase, async: true

  # This was added by Phoenix
  # I comment it out so we have it as an example,
  # but since we haven't changed functionality, we shouldn't have extra tests.
  #test "renders 404" do
  #  assert Yurt.API.RESTAPIWeb.ErrorJSON.render("404.json", %{}) == %{errors: %{detail: "Not Found"}}
  #end

  #test "renders 500" do
  #  assert Yurt.API.RESTAPIWeb.ErrorJSON.render("500.json", %{}) ==
  #           %{errors: %{detail: "Internal Server Error"}}
  #end
end
