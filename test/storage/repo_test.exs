defmodule Yurt.Storage.RepoTest do
  use ExUnit.Case
  doctest Yurt.Storage.Repo

  setup do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Yurt.Storage.Repo)
    :ok = Ecto.Adapters.SQL.Sandbox.mode(Yurt.Storage.Repo, {:shared, self()})
  end

  describe "Storing an account" do
    @user1_username "UwU_01"
    @user1_password_hash "supertrooper"
    @user1_mail_address "my@me"
    @user2_username "betty"
    @user2_password_hash "boobadoop"
    @user2_mail_address "dizzy@dishes"

    # Happy flow
    test "stores a new account and provides an :ok response" do
      assert Yurt.Storage.Repo.store_new_account(
               @user1_username,
               @user1_password_hash,
               @user1_mail_address
             ) == :ok

      # We know that an account can't be stored twice (this is tested in the exception flow),
      # so by trying to store it again, we can be sure that it was indeed stored
      refute Yurt.Storage.Repo.store_new_account(
               @user1_username,
               @user1_password_hash,
               @user1_mail_address
             ) == :ok
    end

    # WARNING! Strongly coupled helper
    # At the time of writing the tests using this, we didn't have a way to check through the Repo behaviour if an account was stored.
    # For now we check in the database directly, but that means our test is strongly coupled to the implementation, and that's bad practice.
    # Try avoiding this helper!
    # If we one day have a function in the Repo behaviour to fetch accounts, an option may be to use that instead.
    # If a test is failing because of this helper, you will need to decide if it's functionaly failing, or if it's a false alarm due to the strong coupling. Then, act accordingly.
    defp accounts_empty?() do
      assert Yurt.Storage.Repo.all(Yurt.Storage.Repo.Account) == []
    end

    # WARNING! Strongly coupled helper
    # At the time of writing the tests using this, we didn't have a way to check through the Repo behaviour if an account was stored.
    # For now we check in the database directly, but that means our test is strongly coupled to the implementation, and that's bad practice.
    # Try avoiding this helper!
    # If we one day have a function in the Repo behaviour to fetch accounts, an option may be to use that instead.
    # If a test is failing because of this helper, you will need to decide if it's functionaly failing, or if it's a false alarm due to the strong coupling. Then, act accordingly.
    defp accounts_contains_only_user1?() do
      assert match?(
               [
                 %Yurt.Storage.Repo.Account{
                   username: @user1_username,
                   password_hash: @user1_password_hash,
                   mail_address: @user1_mail_address
                 }
               ],
               Yurt.Storage.Repo.all(Yurt.Storage.Repo.Account)
             )
    end

    # Exception flow
    test "doesn't store a username that already exists and provides a proper response" do
      assert accounts_empty?()

      assert Yurt.Storage.Repo.store_new_account(
               @user1_username,
               @user1_password_hash,
               @user1_mail_address
             ) == :ok

      accounts_contains_only_user1?()

      assert Yurt.Storage.Repo.store_new_account(
               @user1_username,
               @user2_password_hash,
               @user2_mail_address
             ) == {:error, :username_already_exists}

      accounts_contains_only_user1?()
    end

    test "doesn't store a mail address that already exists and provides a proper response" do
      assert accounts_empty?()

      assert Yurt.Storage.Repo.store_new_account(
               @user1_username,
               @user1_password_hash,
               @user1_mail_address
             ) == :ok

      accounts_contains_only_user1?()

      assert Yurt.Storage.Repo.store_new_account(
               @user2_username,
               @user2_password_hash,
               @user1_mail_address
             ) == {:error, :mail_address_already_exists}

      accounts_contains_only_user1?()
    end
  end
end
