defmodule Yurt.APITest do
  use ExUnit.Case
  doctest Yurt.API

  describe "Registering an account" do
    @new_mail_address "my@me"
    @new_username "UwU_01"
    @safe_password "supertrooper"
    # TODO: This is not the hash !o.o! We need to implement password hashing as well!
    @safe_password_hash "supertrooper"
    @existing_username "existing_user"
    @existing_mail_address "existing@example.org"

    # Happy flow
    test "stores a new account and provides an :ok response" do
      assert Yurt.API.register_account(@new_username, @safe_password, @new_mail_address) == :ok
      assert_received {:store_new_account, @new_username, @safe_password_hash, @new_mail_address}
    end

    # Exception flow
    test "doesn't store a new account when password validation fails and provides a proper response" do
      assert Yurt.API.register_account(@new_username, "toosimple", @new_mail_address) ==
               {:error, :password_validation_failed}

      refute_received {:store_new_account, _, _, _}
    end

    test "doesn't store a new account when mail address validation fails and provides a proper response" do
      assert Yurt.API.register_account(@new_username, @safe_password, "nop") ==
               {:error, :mail_address_validation_failed}

      refute_received {:store_new_account, _, _, _}
    end

    test "doesn't store a new account when username validation fails and provides a proper response" do
      assert Yurt.API.register_account(".?whuu", @safe_password, @new_mail_address) ==
               {:error, :username_validation_failed}

      refute_received {:store_new_account, _, _, _}
    end

    test "gives an error when the username is already taken and provides a proper response" do
      assert Yurt.API.register_account(@existing_username, @safe_password, @new_mail_address) ==
               {:error, :username_already_exists}

      assert_received {:store_new_account, @existing_username, @safe_password_hash,
                       @new_mail_address}
    end

    test "gives an error when the mail address is already taken and provides a proper response" do
      assert Yurt.API.register_account(@new_username, @safe_password, @existing_mail_address) ==
               {:error, :mail_address_already_exists}

      assert_received {:store_new_account, @new_username, @safe_password_hash,
                       @existing_mail_address}
    end
  end
end
